package farmacia.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import farmacia.modelo.personas.Cliente;
import farmacia.modelo.Producto;
import farmacia.modelo.ProductoVendido;
import farmacia.modelo.Venta;

/**
 * Clase para acceder y manipular los datos de las ventas en la base de datos.
 */
public class VentaDAO {
    private final Connection connection;

    /**
     * Constructor de VentaDAO.
     *
     * @param connection Objeto Connection para la conexión a la base de datos.
     */
    public VentaDAO(Connection connection) {
        this.connection = connection;
    }

    /**
     * Agrega una venta a la base de datos.
     *
     * @param venta Objeto Venta a agregar.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public void agregarVenta(Venta venta) throws SQLException {
        String ventaQuery = "INSERT INTO Venta (cliente_dni, total) VALUES (?, ?)";
        String productosVendidosQuery = "INSERT INTO Productos_Vendidos (venta_id, codigo_producto, cantidad) VALUES (?, ?, ?)";

        try (PreparedStatement ventaStatement = connection.prepareStatement(ventaQuery, PreparedStatement.RETURN_GENERATED_KEYS);
             PreparedStatement productosVendidosStatement = connection.prepareStatement(productosVendidosQuery)) {

            ventaStatement.setString(1, venta.getCliente().getDni());
            ventaStatement.setDouble(2, venta.getTotal());
            ventaStatement.executeUpdate();

            ResultSet generatedKeys = ventaStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                int ventaId = generatedKeys.getInt(1);

                for (ProductoVendido productoVendido : venta.getProductos()) {
                    productosVendidosStatement.setInt(1, ventaId);
                    productosVendidosStatement.setString(2, productoVendido.getCodigoProducto());
                    productosVendidosStatement.setInt(3, productoVendido.getCantidad());
                    productosVendidosStatement.executeUpdate();
                }
            }
        }
    }

    public void actualizarVenta(Venta venta) throws SQLException {
        eliminarProductosVendidosPorVentaId(venta.getId());
        agregarVenta(venta);
    }

    /**
     * Elimina los productos vendidos de una venta según su ID de venta.
     *
     * @param ventaId ID de la venta a la que pertenecen los productos vendidos.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public void eliminarProductosVendidosPorVentaId(int ventaId) throws SQLException {
        String query = "DELETE FROM Productos_Vendidos WHERE venta_id = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, ventaId);
            statement.executeUpdate();
        }
    }

    /**
     * Obtiene todos los productos vendidos en una venta según su ID de venta.
     *
     * @param ventaId ID de la venta.
     * @return Lista de productos vendidos en la venta.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public List<ProductoVendido> obtenerProductosVendidosPorVentaId(int ventaId) throws SQLException {
        List<ProductoVendido> productosVendidos = new ArrayList<>();
        String query = "SELECT * FROM Productos_Vendidos WHERE venta_id = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, ventaId);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String codigoProducto = resultSet.getString("codigo_producto");
                int cantidad = resultSet.getInt("cantidad");

                ProductoVendido productoVendido = new ProductoVendido(codigoProducto, cantidad);
                productosVendidos.add(productoVendido);
            }
        }

        return productosVendidos;
    }
}
