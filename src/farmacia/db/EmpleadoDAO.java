package farmacia.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import farmacia.modelo.personas.CredencialesEmpleado;
import farmacia.modelo.personas.Empleado;

/**
 * Clase para acceder y manipular los datos de los empleados en la base de datos.
 */
public class EmpleadoDAO {
    private final Connection connection;

    /**
     * Constructor de EmpleadoDAO.
     *
     * @param connection Objeto Connection para la conexión a la base de datos.
     */
    public EmpleadoDAO(Connection connection) {
        this.connection = connection;
    }

    /**
     * Agrega un empleado a la base de datos.
     *
     * @param empleado Objeto Empleado a agregar.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public void agregarEmpleado(Empleado empleado) throws SQLException {
        String query = "INSERT INTO Empleado (dni, nombre, apellido, telefono, salario, cargo, password) VALUES (?, ?, ?, ?, ?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, empleado.getDni());
            statement.setString(2, empleado.getNombre());
            statement.setString(3, empleado.getApellido());
            statement.setString(4, empleado.getTelefono());
            statement.setDouble(5, empleado.getSalario());
            statement.setString(6, empleado.getCargo());
            statement.setString(7, empleado.getCredenciales().getPassword());
            statement.executeUpdate();
        }
    }

    /**
     * Elimina un empleado de la base de datos según su número de identificación.
     *
     * @param dni Número de identificación del empleado a eliminar.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public void eliminarEmpleado(String dni) throws SQLException {
        String query = "DELETE FROM Empleado WHERE dni = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, dni);
            statement.executeUpdate();
        }
    }

    /**
     * Obtiene todos los empleados de la base de datos.
     *
     * @return Lista de empleados.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public List<Empleado> obtenerTodosLosEmpleados() throws SQLException {
        List<Empleado> empleados = new ArrayList<>();
        String query = "SELECT * FROM Empleado";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String dni = resultSet.getString("dni");
                String nombre = resultSet.getString("nombre");
                String apellido = resultSet.getString("apellido");
                String telefono = resultSet.getString("telefono");
                double salario = resultSet.getDouble("salario");
                String cargo = resultSet.getString("cargo");
                String password = resultSet.getString("password");

                CredencialesEmpleado credenciales = new CredencialesEmpleado(dni, password);
                Empleado empleado = new Empleado(dni, nombre, apellido, telefono, salario, cargo, credenciales);
                empleados.add(empleado);
            }
        }

        return empleados;
    }

    /**
     * Obtiene un empleado de la base de datos según su número de identificación.
     *
     * @param dni Número de identificación del empleado.
     * @return Objeto Empleado si se encuentra, null si no se encuentra.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public Empleado obtenerEmpleadoPorDni(String dni) throws SQLException {
        String query = "SELECT * FROM Empleado WHERE dni = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, dni);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String nombre = resultSet.getString("nombre");
                String apellido = resultSet.getString("apellido");
                String telefono = resultSet.getString("telefono");
                double salario = resultSet.getDouble("salario");
                String cargo = resultSet.getString("cargo");
                String password = resultSet.getString("password");

                CredencialesEmpleado credenciales = new CredencialesEmpleado(dni, password);
                return new Empleado(dni, nombre, apellido, telefono, salario, cargo, credenciales);
            }
        }

        return null;
    }

    /**
     * Obtiene un empleado de la base de datos según sus credenciales (dni y contraseña).
     *
     * @param dni      Número de identificación del empleado.
     * @param password Contraseña del empleado.
     * @return Objeto Empleado si se encuentra, null si no se encuentra.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public Empleado obtenerEmpleadoPorCredenciales(String dni, String password) throws SQLException {
        String query = "SELECT * FROM Empleado WHERE dni = ? AND password = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, dni);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String nombre = resultSet.getString("nombre");
                String apellido = resultSet.getString("apellido");
                String telefono = resultSet.getString("telefono");
                double salario = resultSet.getDouble("salario");
                String cargo = resultSet.getString("cargo");

                CredencialesEmpleado credenciales = new CredencialesEmpleado(dni, password);
                return new Empleado(dni, nombre, apellido, telefono, salario, cargo, credenciales);
            }
        }

        return null;
    }
    
    /**
    * Actualiza un empleado en la base de datos.
    *
    * @param empleado Objeto Empleado actualizado.
    * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
    */
    public void actualizarEmpleado(Empleado empleado) throws SQLException {
        String query = "UPDATE Empleado SET nombre = ?, apellido = ?, telefono = ?, salario = ?, cargo = ?, password = ? WHERE dni = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, empleado.getNombre());
            statement.setString(2, empleado.getApellido());
            statement.setString(3, empleado.getTelefono());
            statement.setDouble(4, empleado.getSalario());
            statement.setString(5, empleado.getCargo());
            statement.setString(6, empleado.getCredenciales().getPassword());
            statement.setString(7, empleado.getDni());
            statement.executeUpdate();
        }
    }
}