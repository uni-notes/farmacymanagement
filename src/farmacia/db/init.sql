DROP TABLE IF EXISTS Productos_Vendidos CASCADE;
DROP TABLE IF EXISTS Venta CASCADE;
DROP TABLE IF EXISTS Cliente CASCADE;
DROP TABLE IF EXISTS Producto CASCADE;
DROP TABLE IF EXISTS Empleado CASCADE;

CREATE TABLE Empleado (
  dni VARCHAR(8) NOT NULL,
  nombre VARCHAR(100) NOT NULL,
  apellido VARCHAR(100) NOT NULL,
  telefono VARCHAR(20) NOT NULL,
  salario DOUBLE PRECISION NOT NULL,
  cargo VARCHAR(100) NOT NULL,
  password VARCHAR(100),
  PRIMARY KEY (dni)
);

CREATE TABLE Producto (
  codigo VARCHAR(20) NOT NULL,
  nombre VARCHAR(100) NOT NULL,
  precio DOUBLE PRECISION NOT NULL,
  cantidad INT NOT NULL,
  PRIMARY KEY (codigo)
);

CREATE TABLE Cliente (
  dni VARCHAR(8) NOT NULL,
  nombre VARCHAR(100) NOT NULL,
  apellido VARCHAR(100) NOT NULL,
  telefono VARCHAR(20) NOT NULL,
  direccion VARCHAR(100) NOT NULL,
  correo VARCHAR(100) NOT NULL,
  PRIMARY KEY (dni)
);

CREATE TABLE Venta (
  venta_id SERIAL PRIMARY KEY,
  cliente_dni VARCHAR(8) NOT NULL,
  total DOUBLE PRECISION NOT NULL,
  FOREIGN KEY (cliente_dni) REFERENCES Cliente(dni)
);

CREATE TABLE Productos_Vendidos (
  id SERIAL PRIMARY KEY,
  venta_id INT NOT NULL,
  codigo_producto VARCHAR(20) NOT NULL,
  cantidad INT NOT NULL,
  FOREIGN KEY (venta_id) REFERENCES Venta(venta_id),
  FOREIGN KEY (codigo_producto) REFERENCES Producto(codigo)
);

INSERT INTO Empleado (dni, nombre, apellido, telefono, salario, cargo, password)
VALUES 
  ('71504619', 'Cesar', 'Caycho', '999999999', 4000.0, 'Desarrollador', '12345'),
  ('47019177', 'Yosinao', 'Marquina', '987654321', 2500.0, 'Desarrollador', '123456'),
  ('33333333', 'Empleado 3', 'Apellido 3', '555555555', 3000.0, 'Cargo 3', 'password3');

INSERT INTO Producto (codigo, nombre, precio, cantidad)
VALUES ('P1', 'Producto 1', 10.0, 50),
       ('P2', 'Producto 2', 20.0, 100),
       ('P3', 'Producto 3', 30.0, 200);

INSERT INTO Cliente (dni, nombre, apellido, telefono, direccion, correo)
VALUES 
  ('71504619', 'Cesar', 'Caycho', '999999999', 'Dirección 1', 'Correo 1'),
  ('55555555', 'Cliente 2', 'Apellido 2', '987654321', 'Dirección 2', 'Correo 2'),
  ('66666666', 'Cliente 3', 'Apellido 3', '555555555', 'Dirección 3', 'Correo 3');
