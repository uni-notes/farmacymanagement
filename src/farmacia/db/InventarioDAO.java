package farmacia.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import farmacia.modelo.Inventario;
import farmacia.modelo.Producto;

public class InventarioDAO {
    private final Connection connection;

    public InventarioDAO(Connection connection) {
        this.connection = connection;
    }

    public void agregarInventario(Inventario inventario) throws SQLException {
        String query = "INSERT INTO Inventario (producto_codigo, cantidad) VALUES (?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, inventario.getProducto().getCodigo());
            statement.setInt(2, inventario.getCantidad());
            statement.executeUpdate();
        }
    }

    public void eliminarInventario(String codigoProducto) throws SQLException {
        String query = "DELETE FROM Inventario WHERE producto_codigo = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, codigoProducto);
            statement.executeUpdate();
        }
    }

    public List<Inventario> obtenerTodosLosInventarios() throws SQLException {
        List<Inventario> inventarios = new ArrayList<>();
        String query = "SELECT * FROM Inventario";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String codigoProducto = resultSet.getString("producto_codigo");
                int cantidad = resultSet.getInt("cantidad");

                Producto producto = obtenerProductoPorCodigo(codigoProducto);

                if (producto != null) {
                    Inventario inventario = new Inventario(producto, cantidad);
                    inventarios.add(inventario);
                }
            }
        }

        return inventarios;
    }

    public Inventario obtenerInventarioPorCodigoProducto(String codigoProducto) throws SQLException {
        String query = "SELECT * FROM Inventario WHERE producto_codigo = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, codigoProducto);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                int cantidad = resultSet.getInt("cantidad");

                Producto producto = obtenerProductoPorCodigo(codigoProducto);

                if (producto != null) {
                    return new Inventario(producto, cantidad);
                }
            }
        }

        return null;
    }

    private Producto obtenerProductoPorCodigo(String codigoProducto) throws SQLException {
        String query = "SELECT * FROM Producto WHERE codigo = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, codigoProducto);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String nombre = resultSet.getString("nombre");
                double precio = resultSet.getDouble("precio");
                int cantidad = resultSet.getInt("cantidad");

                return new Producto(codigoProducto, nombre, precio, cantidad);
            }
        }

        return null;
    }
}