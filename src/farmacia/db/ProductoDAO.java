package farmacia.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import farmacia.modelo.Producto;

/**
 * Clase para acceder y manipular los datos de los productos en la base de datos.
 */
public class ProductoDAO {
    private final Connection connection;

    /**
     * Constructor de ProductoDAO.
     *
     * @param connection Objeto Connection para la conexión a la base de datos.
     */
    public ProductoDAO(Connection connection) {
        this.connection = connection;
    }

    /**
     * Agrega un producto a la base de datos.
     *
     * @param producto Objeto Producto a agregar.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public void agregarProducto(Producto producto) throws SQLException {
        String query = "INSERT INTO Producto (codigo, nombre, precio, cantidad) VALUES (?, ?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, producto.getCodigo());
            statement.setString(2, producto.getNombre());
            statement.setDouble(3, producto.getPrecio());
            statement.setInt(4, producto.getCantidad());
            statement.executeUpdate();
        }
    }

    /**
     * Elimina un producto de la base de datos según su código.
     *
     * @param codigo Código del producto a eliminar.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public void eliminarProducto(String codigo) throws SQLException {
        String query = "DELETE FROM Producto WHERE codigo = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, codigo);
            statement.executeUpdate();
        }
    }

    /**
     * Obtiene todos los productos de la base de datos.
     *
     * @return Lista de productos.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public List<Producto> obtenerTodosLosProductos() throws SQLException {
        List<Producto> productos = new ArrayList<>();
        String query = "SELECT * FROM Producto";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String codigo = resultSet.getString("codigo");
                String nombre = resultSet.getString("nombre");
                double precio = resultSet.getDouble("precio");
                int cantidad = resultSet.getInt("cantidad");

                Producto producto = new Producto(codigo, nombre, precio, cantidad);
                productos.add(producto);
            }
        }

        return productos;
    }

    /**
     * Obtiene un producto de la base de datos según su código.
     *
     * @param codigo Código del producto.
     * @return Objeto Producto si se encuentra, null si no se encuentra.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public Producto obtenerProductoPorCodigo(String codigo) throws SQLException {
        String query = "SELECT * FROM Producto WHERE codigo = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, codigo);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String nombre = resultSet.getString("nombre");
                double precio = resultSet.getDouble("precio");
                int cantidad = resultSet.getInt("cantidad");

                return new Producto(codigo, nombre, precio, cantidad);
            }
        }

        return null;
    }
    
    /**
    * Actualiza un producto en la base de datos.
    *
    * @param producto Objeto Producto actualizado.
    * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
    */
    public void actualizarProducto(Producto producto) throws SQLException {
        String query = "UPDATE Producto SET nombre = ?, precio = ?, cantidad = ? WHERE codigo = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, producto.getNombre());
            statement.setDouble(2, producto.getPrecio());
            statement.setInt(3, producto.getCantidad());
            statement.setString(4, producto.getCodigo());
            statement.executeUpdate();
        }
    }
}