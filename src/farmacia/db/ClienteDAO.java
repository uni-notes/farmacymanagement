package farmacia.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import farmacia.modelo.personas.Cliente;

/**
 * DAO (Data Access Object) para la entidad Cliente.
 * Permite interactuar con la tabla Cliente en la base de datos.
 */
public class ClienteDAO {

    private final Connection connection;

    /**
     * Constructor de ClienteDAO.
     * 
     * @param connection Objeto Connection para la conexión a la base de datos.
     */
    public ClienteDAO(Connection connection) {
        this.connection = connection;
    }

    /**
     * Agrega un nuevo cliente a la base de datos.
     * 
     * @param cliente Cliente a agregar.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public void agregarCliente(Cliente cliente) throws SQLException {
        String query = "INSERT INTO Cliente (dni, nombre, apellido, telefono, direccion, correo) VALUES (?, ?, ?, ?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, cliente.getDni());
            statement.setString(2, cliente.getNombre());
            statement.setString(3, cliente.getApellido());
            statement.setString(4, cliente.getTelefono());
            statement.setString(5, cliente.getDireccion());
            statement.setString(6, cliente.getCorreo());
            statement.executeUpdate();
        }
    }

    /**
     * Elimina un cliente de la base de datos por su DNI.
     * 
     * @param dni DNI del cliente a eliminar.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public void eliminarCliente(String dni) throws SQLException {
        String query = "DELETE FROM Cliente WHERE dni = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, dni);
            statement.executeUpdate();
        }
    }

    /**
     * Obtiene todos los clientes de la base de datos.
     * 
     * @return Lista de clientes si existen, una lista vacía si la tabla de clientes está vacía.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public List<Cliente> obtenerTodosLosClientes() throws SQLException {
        List<Cliente> clientes = new ArrayList<>();
        String query = "SELECT * FROM Cliente";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String dni = resultSet.getString("dni");
                String nombre = resultSet.getString("nombre");
                String apellido = resultSet.getString("apellido");
                String telefono = resultSet.getString("telefono");
                String direccion = resultSet.getString("direccion");
                String correo = resultSet.getString("correo");

                Cliente cliente = new Cliente(dni, nombre, apellido, telefono, direccion, correo);
                clientes.add(cliente);
            }
        }

        return clientes;
    }

    /**
     * Obtiene un cliente de la base de datos por su DNI.
     * 
     * @param dni DNI del cliente a buscar.
     * @return Cliente si se encuentra, null si no se encuentra.
     * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
     */
    public Cliente obtenerClientePorDni(String dni) throws SQLException {
        String query = "SELECT * FROM Cliente WHERE dni = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, dni);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String nombre = resultSet.getString("nombre");
                String apellido = resultSet.getString("apellido");
                String telefono = resultSet.getString("telefono");
                String direccion = resultSet.getString("direccion");
                String correo = resultSet.getString("correo");

                return new Cliente(dni, nombre, apellido, telefono, direccion, correo);
            }
        }

        return null;
    }
    
    /**
    * Actualiza un cliente en la base de datos.
    * 
    * @param cliente Cliente a actualizar.
    * @throws SQLException Si ocurre un error al ejecutar la consulta SQL.
    */
    public void actualizarCliente(Cliente cliente) throws SQLException {
        String query = "UPDATE Cliente SET nombre = ?, apellido = ?, telefono = ?, direccion = ?, correo = ? WHERE dni = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, cliente.getNombre());
            statement.setString(2, cliente.getApellido());
            statement.setString(3, cliente.getTelefono());
            statement.setString(4, cliente.getDireccion());
            statement.setString(5, cliente.getCorreo());
            statement.setString(6, cliente.getDni());
            statement.executeUpdate();
        }
    }
}