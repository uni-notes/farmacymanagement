package farmacia.db;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Clase para administrar la conexión y operaciones con la base de datos.
 */
public class DB {
    private static final String URL = "jdbc:postgresql://localhost:5432/farmacia";
    private static final String USERNAME = "cfabrica46";
    private static final String PASSWORD = "Xtron46_YT";
    private static final String INIT_SQL_PATH = "farmacia/db/init.sql";

    private Connection connection;

    /**
     * Constructor de DB.
     * Intenta establecer una conexión con la base de datos.
     */
    public DB() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            System.out.println("Conexión establecida a la base de datos");
        } catch (ClassNotFoundException e) {
            System.out.println("Error al cargar el controlador de la base de datos");
            System.out.println(e);
        } catch (SQLException e) {
            System.out.println("Error al conectar a la base de datos");
            System.out.println(e);
        }
    }

    /**
     * Obtiene la conexión actual a la base de datos.
     * 
     * @return Objeto Connection para la conexión.
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Verifica si la conexión a la base de datos está activa.
     * 
     * @return true si la conexión está activa, false de lo contrario.
     */
    public boolean ping() {
        try {
            return connection != null && !connection.isClosed();
        } catch (SQLException e) {
            System.out.println(e);
            return false;
        }
    }

    /**
     * Inicializa la base de datos ejecutando el script de inicialización.
     */
    public void initDB() {
        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(INIT_SQL_PATH);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            Statement statement = connection.createStatement();
            String line;
            StringBuilder script = new StringBuilder();

            while ((line = reader.readLine()) != null) {
                script.append(line);
                // Si la línea contiene un punto y coma, se ejecuta el script acumulado
                if (line.trim().endsWith(";")) {
                    statement.execute(script.toString());
                    script.setLength(0); // Reiniciar el script acumulado
                }
            }

            System.out.println("Script ejecutado correctamente");
            statement.close();
            reader.close();
        } catch (Exception e) {
            System.out.println("Error al ejecutar el script de inicialización");
            System.out.println(e);
        }
    }
}