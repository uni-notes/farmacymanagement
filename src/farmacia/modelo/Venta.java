package farmacia.modelo;

import java.util.ArrayList;
import java.util.List;

import farmacia.modelo.personas.Cliente;
import farmacia.modelo.ProductoVendido;

/**
 * Clase que representa una venta realizada en la farmacia.
 */
public class Venta {
    private int id;
    private Cliente cliente;
    private List<ProductoVendido> productos;
    private double total;

    /**
     * Constructor de la clase Venta.
     *
     * @param id         ID de la venta.
     * @param cliente    Cliente que realizó la venta.
     * @param productos  Lista de productos vendidos.
     * @param total      Total de la venta.
     */
    public Venta(Cliente cliente, List<ProductoVendido> productos, double total) {
        this.cliente = cliente;
        this.productos = productos;
        this.total = total;
    }

    /**
     * Obtiene el ID de la venta.
     *
     * @return ID de la venta.
     */
    public int getId() {
        return id;
    }

    /**
     * Establece el ID de la venta.
     *
     * @param id ID de la venta.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Obtiene el cliente que realizó la venta.
     *
     * @return Cliente que realizó la venta.
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * Establece el cliente que realizó la venta.
     *
     * @param cliente Cliente que realizó la venta.
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * Obtiene la lista de productos vendidos.
     *
     * @return Lista de productos vendidos.
     */
    public List<ProductoVendido> getProductos() {
        return productos;
    }

    /**
     * Establece la lista de productos vendidos.
     *
     * @param productos Lista de productos vendidos.
     */
    public void setProductos(List<ProductoVendido> productos) {
        this.productos = productos;
    }

    /**
     * Agrega un producto vendido a la lista de productos.
     *
     * @param productoVendido Producto vendido a agregar.
     */
    public void agregarProductoVendido(ProductoVendido productoVendido) {
        if (productos == null) {
            productos = new ArrayList<>();
        }
        productos.add(productoVendido);
    }

    /**
     * Obtiene el total de la venta.
     *
     * @return Total de la venta.
     */
    public double getTotal() {
        return total;
    }

    /**
     * Establece el total de la venta.
     *
     * @param total Total de la venta.
     */
    public void setTotal(double total) {
        this.total = total;
    }
}
