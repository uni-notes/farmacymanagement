package farmacia.modelo;

/**
 * Clase que representa un producto vendido en una venta.
 */
public class ProductoVendido {
    private int id;
    private String codigoProducto;
    private int cantidad;

    /**
     * Constructor de la clase ProductoVendido.
     *
     * @param id            ID del producto vendido.
     * @param codigoProducto Código del producto vendido.
     * @param cantidad      Cantidad del producto vendido.
     */
    public ProductoVendido(String codigoProducto, int cantidad) {
        this.codigoProducto = codigoProducto;
        this.cantidad = cantidad;
    }

    /**
     * Obtiene el ID del producto vendido.
     *
     * @return ID del producto vendido.
     */
    public int getId() {
        return id;
    }

    /**
     * Establece el ID del producto vendido.
     *
     * @param id ID del producto vendido.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Obtiene el código del producto vendido.
     *
     * @return Código del producto vendido.
     */
    public String getCodigoProducto() {
        return codigoProducto;
    }

    /**
     * Establece el código del producto vendido.
     *
     * @param codigoProducto Código del producto vendido.
     */
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    /**
     * Obtiene la cantidad del producto vendido.
     *
     * @return Cantidad del producto vendido.
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * Establece la cantidad del producto vendido.
     *
     * @param cantidad Cantidad del producto vendido.
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
