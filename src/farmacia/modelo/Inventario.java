package farmacia.modelo;

/**
 * Clase que representa un elemento del inventario de la farmacia.
 */
public class Inventario {
    private Producto producto;
    private int cantidad;

    /**
     * Constructor de la clase Inventario.
     *
     * @param producto  Producto asociado al elemento del inventario.
     * @param cantidad  Cantidad disponible del producto en el inventario.
     */
    public Inventario(Producto producto, int cantidad) {
        this.producto = producto;
        this.cantidad = cantidad;
    }

    /**
     * Obtiene el producto asociado al elemento del inventario.
     *
     * @return Producto asociado.
     */
    public Producto getProducto() {
        return producto;
    }

    /**
     * Establece el producto asociado al elemento del inventario.
     *
     * @param producto Producto asociado.
     */
    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    /**
     * Obtiene la cantidad disponible del producto en el inventario.
     *
     * @return Cantidad disponible del producto.
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * Establece la cantidad disponible del producto en el inventario.
     *
     * @param cantidad Cantidad disponible del producto.
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}