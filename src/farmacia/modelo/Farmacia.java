package farmacia.modelo;

import java.util.ArrayList;
import java.util.List;

import farmacia.modelo.personas.Cliente;
import farmacia.modelo.personas.Empleado;

/**
 * Clase que representa una farmacia.
 */
public class Farmacia {
    private String nombre;
    private String direccion;
    private String telefono;
    private final List<Empleado> empleados;
    private final List<Producto> productos;
    private final List<Cliente> clientes;

    /**
     * Constructor de la clase Farmacia.
     *
     * @param nombre    Nombre de la farmacia.
     * @param direccion Dirección de la farmacia.
     * @param telefono  Teléfono de la farmacia.
     */
    public Farmacia(String nombre, String direccion, String telefono) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.empleados = new ArrayList<>();
        this.productos = new ArrayList<>();
        this.clientes = new ArrayList<>();
    }

    /**
     * Obtiene el nombre de la farmacia.
     *
     * @return Nombre de la farmacia.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Establece el nombre de la farmacia.
     *
     * @param nombre Nombre de la farmacia.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Obtiene la dirección de la farmacia.
     *
     * @return Dirección de la farmacia.
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Establece la dirección de la farmacia.
     *
     * @param direccion Dirección de la farmacia.
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Obtiene el teléfono de la farmacia.
     *
     * @return Teléfono de la farmacia.
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Establece el teléfono de la farmacia.
     *
     * @param telefono Teléfono de la farmacia.
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * Agrega un empleado a la lista de empleados de la farmacia.
     *
     * @param empleado Empleado a agregar.
     */
    public void agregarEmpleado(Empleado empleado) {
        empleados.add(empleado);
    }

    /**
     * Elimina un empleado de la lista de empleados de la farmacia.
     *
     * @param empleado Empleado a eliminar.
     */
    public void eliminarEmpleado(Empleado empleado) {
        empleados.remove(empleado);
    }

    /**
     * Agrega un producto a la lista de productos de la farmacia.
     *
     * @param producto Producto a agregar.
     */
    public void agregarProducto(Producto producto) {
        productos.add(producto);
    }

    /**
     * Elimina un producto de la lista de productos de la farmacia.
     *
     * @param producto Producto a eliminar.
     */
    public void eliminarProducto(Producto producto) {
        productos.remove(producto);
    }

    /**
     * Agrega un cliente a la lista de clientes de la farmacia.
     *
     * @param cliente Cliente a agregar.
     */
    public void agregarCliente(Cliente cliente) {
        clientes.add(cliente);
    }

    /**
     * Elimina un cliente de la lista de clientes de la farmacia.
     *
     * @param cliente Cliente a eliminar.
     */
    public void eliminarCliente(Cliente cliente){
        clientes.remove(cliente);
    }
}