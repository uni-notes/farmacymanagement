package farmacia.modelo.personas;

/**
 * Clase que representa un cliente en la farmacia.
 */
public class Cliente extends Persona {
    private String direccion;
    private String correo;

    /**
     * Constructor de la clase Cliente.
     *
     * @param dni       DNI del cliente.
     * @param nombre    Nombre del cliente.
     * @param apellido  Apellido del cliente.
     * @param telefono  Teléfono del cliente.
     * @param direccion Dirección del cliente.
     * @param correo    Correo electrónico del cliente.
     */
    public Cliente(String dni, String nombre, String apellido, String telefono, String direccion, String correo) {
        super(dni, nombre, apellido, telefono);
        this.direccion = direccion;
        this.correo = correo;
    }

    /**
     * Obtiene la dirección del cliente.
     *
     * @return Dirección del cliente.
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Establece la dirección del cliente.
     *
     * @param direccion Dirección del cliente.
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Obtiene el correo electrónico del cliente.
     *
     * @return Correo electrónico del cliente.
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Establece el correo electrónico del cliente.
     *
     * @param correo Correo electrónico del cliente.
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }
}