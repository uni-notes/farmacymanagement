package farmacia.modelo.personas;

/**
 * Clase que representa a un empleado.
 */
public class Empleado extends Persona {
    private double salario;
    private String cargo;
    private CredencialesEmpleado credenciales;

    /**
     * Constructor de la clase Empleado.
     *
     * @param dni          DNI del empleado.
     * @param nombre       Nombre del empleado.
     * @param apellido     Apellido del empleado.
     * @param telefono     Teléfono del empleado.
     * @param salario      Salario del empleado.
     * @param cargo        Cargo del empleado.
     * @param credenciales Credenciales del empleado.
     */
    public Empleado(String dni, String nombre, String apellido, String telefono, double salario, String cargo, CredencialesEmpleado credenciales) {
        super(dni, nombre, apellido, telefono);
        this.salario = salario;
        this.cargo = cargo;
        this.credenciales = credenciales;
    }

    /**
     * Obtiene el salario del empleado.
     *
     * @return Salario del empleado.
     */
    public double getSalario() {
        return salario;
    }

    /**
     * Establece el salario del empleado.
     *
     * @param salario Salario del empleado.
     */
    public void setSalario(double salario) {
        this.salario = salario;
    }

    /**
     * Obtiene el cargo del empleado.
     *
     * @return Cargo del empleado.
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * Establece el cargo del empleado.
     *
     * @param cargo Cargo del empleado.
     */
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    /**
     * Obtiene las credenciales del empleado.
     *
     * @return Credenciales del empleado.
     */
    public CredencialesEmpleado getCredenciales() {
        return credenciales;
    }

    /**
     * Establece las credenciales del empleado.
     *
     * @param credenciales Credenciales del empleado.
     */
    public void setCredenciales(CredencialesEmpleado credenciales) {
        this.credenciales = credenciales;
    }
}