package farmacia.modelo.personas;

/**
 * Clase que representa las credenciales de un empleado.
 */
public class CredencialesEmpleado {
    private String username;
    private String password;

    /**
     * Constructor de la clase CredencialesEmpleado.
     *
     * @param username Nombre de usuario del empleado.
     * @param password Contraseña del empleado.
     */
    public CredencialesEmpleado(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Obtiene el nombre de usuario del empleado.
     *
     * @return Nombre de usuario del empleado.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Establece el nombre de usuario del empleado.
     *
     * @param username Nombre de usuario del empleado.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Obtiene la contraseña del empleado.
     *
     * @return Contraseña del empleado.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Establece la contraseña del empleado.
     *
     * @param password Contraseña del empleado.
     */
    public void setPassword(String password) {
        this.password = password;
    }
}