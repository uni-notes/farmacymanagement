package farmacia.modelo.personas;

/**
 * Clase abstracta que representa a una persona.
 */
public abstract class Persona {
    private String dni;
    private String nombre;
    private String apellido;
    private String telefono;

    /**
     * Constructor de la clase Persona.
     *
     * @param dni      DNI de la persona.
     * @param nombre   Nombre de la persona.
     * @param apellido Apellido de la persona.
     * @param telefono Teléfono de la persona.
     */
    protected Persona(String dni, String nombre, String apellido, String telefono) {
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
    }

    /**
     * Obtiene el DNI de la persona.
     *
     * @return DNI de la persona.
     */
    public String getDni() {
        return dni;
    }

    /**
     * Establece el DNI de la persona.
     *
     * @param dni DNI de la persona.
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * Obtiene el nombre de la persona.
     *
     * @return Nombre de la persona.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Establece el nombre de la persona.
     *
     * @param nombre Nombre de la persona.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Obtiene el apellido de la persona.
     *
     * @return Apellido de la persona.
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * Establece el apellido de la persona.
     *
     * @param apellido Apellido de la persona.
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * Obtiene el teléfono de la persona.
     *
     * @return Teléfono de la persona.
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Establece el teléfono de la persona.
     *
     * @param telefono Teléfono de la persona.
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}