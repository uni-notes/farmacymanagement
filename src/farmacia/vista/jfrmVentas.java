/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package farmacia.vista;

import farmacia.controlador.ClienteController;
import farmacia.controlador.EmpleadoController;
import farmacia.controlador.ProductoController;
import farmacia.controlador.VentaController;
import farmacia.modelo.Producto;
import farmacia.modelo.ProductoVendido;
import farmacia.modelo.personas.Cliente;
import static farmacia.vista.jfrmMenu.usuario;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;
import java.util.List;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author xH1t0k1r1x
 */
public class jfrmVentas extends javax.swing.JFrame {

    private static ClienteController clienteController;
    private static EmpleadoController empleadoController;
    private static ProductoController productoController;
    private static VentaController ventaController;

    public static String usuario;

    public static Producto productoMain;
    public static Cliente clienteMain;
    public static int cantidadMain;
    public static double totalMain;

    private static Error ventanaError;
    private static jfrmImpresion impresion;

    public jfrmVentas(String usuario, ClienteController clienteController, EmpleadoController empleadoController, ProductoController productoController, VentaController ventaController) {

        initComponents();
        int red = 170; // Valor de componente rojo
        int green = 210; // Valor de componente verde
        int blue = 255;
        Color color = new Color(red, green, blue);
        this.getContentPane().setBackground(color);
        jfrmVentas.clienteController = clienteController;
        jfrmVentas.empleadoController = empleadoController;
        jfrmVentas.productoController = productoController;
        jfrmVentas.ventaController = ventaController;
        jfrmVentas.usuario = usuario;
        jfrmVentas.ventanaError = new Error();
        jfrmVentas.impresion = new jfrmImpresion();

        txtCode.setEnabled(false);
        txtCantidad.setEnabled(false);
        btnValidarProducto.setEnabled(false);
        btnAgregar.setEnabled(false);
        btnQuitar.setEnabled(false);
        btnVoucher.setEnabled(false);
        btnNuevo.setEnabled(false);
        btnValidarCantidad.setEnabled(false);
        
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        lblLogo = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblSalir = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnValidarCliente = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        txtDni = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtCode = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        btnValidarProducto = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        labelPrecio = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        labelPrecioTotal = new javax.swing.JLabel();
        labelProducto = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        labelStock = new javax.swing.JLabel();
        labelCliente = new javax.swing.JLabel();
        btnValidarCantidad = new javax.swing.JButton();
        btnAgregar = new javax.swing.JButton();
        btnQuitar = new javax.swing.JButton();
        btnVoucher = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        labelTotal = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(153, 255, 153));

        lblLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/farmacia/vista/icons/cliente/LOGO.png"))); // NOI18N

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/farmacia/vista/icons/ventas/logo.png"))); // NOI18N
        jLabel8.setText("VENTAS");

        lblSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/farmacia/vista/icons/cliente/SALIR.png"))); // NOI18N

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/farmacia/vista/icons/cliente/HOME.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(lblLogo)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel8)
                        .addGap(480, 480, 480)))
                .addComponent(lblSalir)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addGap(25, 25, 25))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblLogo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblSalir)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8))
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cod.", "Nombre", "Precio und.", "Precio x cant", "Cantidad"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.Double.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(table);
        if (table.getColumnModel().getColumnCount() > 0) {
            table.getColumnModel().getColumn(0).setResizable(false);
            table.getColumnModel().getColumn(1).setResizable(false);
            table.getColumnModel().getColumn(2).setResizable(false);
            table.getColumnModel().getColumn(3).setResizable(false);
            table.getColumnModel().getColumn(4).setResizable(false);
        }

        jScrollPane2.setViewportView(jScrollPane1);

        jPanel2.setBackground(new java.awt.Color(170, 210, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "INGRESAR DATOS DE VENTA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));

        jLabel1.setText("Ingrese DNI Cliente:");

        btnValidarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/farmacia/vista/icons/ventas/validar.png"))); // NOI18N
        btnValidarCliente.setText("Validar Cliente");
        btnValidarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidarClienteActionPerformed(evt);
            }
        });

        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/farmacia/vista/icons/ventas/agregar.png"))); // NOI18N
        btnNuevo.setText("Nuevo Cliente");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        txtDni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDniKeyTyped(evt);
            }
        });

        jLabel2.setText("Validacion de Cliente:");

        jLabel3.setText("Codigo del producto:");

        jLabel4.setText("Validar producto:");

        jLabel6.setText("Ingrese cantidad de compra:");

        txtCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCantidadActionPerformed(evt);
            }
        });
        txtCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantidadKeyTyped(evt);
            }
        });

        btnValidarProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/farmacia/vista/icons/ventas/agregar.png"))); // NOI18N
        btnValidarProducto.setText("Validar Producto");
        btnValidarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidarProductoActionPerformed(evt);
            }
        });

        jLabel7.setText("Precio:");

        labelPrecio.setText("mostrar precio");

        jLabel10.setText("Precio x total:");

        labelPrecioTotal.setText("mostrar precio");

        labelProducto.setText("mostrar nombre de producto");

        jLabel13.setText("Stock:");

        labelStock.setText("Mostrar stock del producto");

        labelCliente.setText("Mostrar nombre completo del cliente");

        btnValidarCantidad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/farmacia/vista/icons/ventas/validar.png"))); // NOI18N
        btnValidarCantidad.setText("Validar ");
        btnValidarCantidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnValidarCantidadActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addGap(23, 23, 23))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel13)
                            .addComponent(jLabel6)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtDni, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnValidarCliente)
                        .addGap(18, 18, 18)
                        .addComponent(btnNuevo))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(btnValidarProducto))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnValidarCantidad)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel10))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(15, 15, 15)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelStock)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(labelProducto)
                                        .addGap(55, 55, 55)
                                        .addComponent(jLabel7)))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelPrecioTotal)
                            .addComponent(labelPrecio)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(labelCliente)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(btnValidarCliente)
                    .addComponent(btnNuevo)
                    .addComponent(txtDni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(100, 100, 100)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(labelCliente))
                .addGap(76, 76, 76)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnValidarProducto)
                    .addComponent(jLabel3))
                .addGap(25, 25, 25)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(labelProducto)
                    .addComponent(jLabel7)
                    .addComponent(labelPrecio))
                .addGap(31, 31, 31)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(labelStock))
                .addGap(34, 34, 34)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(labelPrecioTotal)
                    .addComponent(btnValidarCantidad))
                .addContainerGap(37, Short.MAX_VALUE))
        );

        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/farmacia/vista/icons/ventas/añadir.png"))); // NOI18N
        btnAgregar.setText("Agregar");
        btnAgregar.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        btnQuitar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/farmacia/vista/icons/ventas/quitar.png"))); // NOI18N
        btnQuitar.setText("Quitar");
        btnQuitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarActionPerformed(evt);
            }
        });

        btnVoucher.setIcon(new javax.swing.ImageIcon(getClass().getResource("/farmacia/vista/icons/ventas/voucher.png"))); // NOI18N
        btnVoucher.setText("Generar Voucher");
        btnVoucher.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoucherActionPerformed(evt);
            }
        });

        jLabel18.setText("Total:");

        labelTotal.setText("Mostrar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnQuitar, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 554, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnVoucher))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel18)
                        .addGap(30, 30, 30)
                        .addComponent(labelTotal)
                        .addGap(147, 147, 147))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(62, 62, 62)
                                .addComponent(btnAgregar)
                                .addGap(72, 72, 72)
                                .addComponent(btnQuitar))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(labelTotal))
                        .addGap(26, 26, 26)
                        .addComponent(btnVoucher)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // Crear una nueva instancia de la ventana jfrmMenu
        var ventana = new jfrmMenu(usuario, clienteController, empleadoController, productoController, ventaController);
        ventana.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCantidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCantidadActionPerformed

    private void btnValidarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidarClienteActionPerformed
        // TODO add your handling code here:
        Cliente cliente = this.obtenerClientePorDNI();
        btnNuevo.setEnabled(true);
        if (cliente != null) {
            txtCode.setEnabled(true);
            btnValidarProducto.setEnabled(true);
            labelCliente.setText(cliente.getNombre());
            btnValidarCliente.setEnabled(false);
            txtDni.setEnabled(false);
            btnNuevo.setEnabled(true);
            
            clienteMain = cliente;
        }



    }//GEN-LAST:event_btnValidarClienteActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        limpiar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnValidarCantidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidarCantidadActionPerformed
        // TODO add your handling code here:
        int cantidad = this.convertirStringAInt(txtCantidad.getText());

        if (cantidad > productoMain.getCantidad() || cantidad == -1) {
            ventanaError.renderMensaje("Cantidad Invalida");
            return;
        }

        double precioTotal = cantidad * productoMain.getPrecio();
        cantidadMain = cantidad;

        labelPrecioTotal.setText(Double.toString(precioTotal));

        btnAgregar.setEnabled(true);
        btnQuitar.setEnabled(true);
    }//GEN-LAST:event_btnValidarCantidadActionPerformed

    private void btnValidarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnValidarProductoActionPerformed
        // TODO add your handling code here:
        Producto producto = this.obtenerProductoPorCodigo();
        if (producto != null) {
            productoMain = producto;

            txtCantidad.setEnabled(true);
            btnValidarCantidad.setEnabled(true);
            labelProducto.setText(producto.getNombre());
            labelPrecio.setText(Double.toString(producto.getPrecio()));
            labelStock.setText(Integer.toString(producto.getCantidad()));
        }
    }//GEN-LAST:event_btnValidarProductoActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        // TODO add your handling code here:
        DefaultTableModel model = (DefaultTableModel) table.getModel();

        int cantidad = this.convertirStringAInt(txtCantidad.getText());
        double precioTotal = cantidad * productoMain.getPrecio();

        totalMain += precioTotal;
        
        labelTotal.setText(Double.toString(totalMain));

        Object[] rowData = {productoMain.getCodigo(), productoMain.getNombre(), productoMain.getPrecio(), precioTotal, cantidadMain};
        model.addRow(rowData);

        btnVoucher.setEnabled(true);
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnQuitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarActionPerformed
        // TODO add your handling code here:
        eliminarUltimaFila();
    }//GEN-LAST:event_btnQuitarActionPerformed

    private void btnVoucherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoucherActionPerformed
        // TODO add your handling code here:
        // Obtener los datos de la tabla
        List<ProductoVendido> productosVendidos = obtenerProductosVendidosDeTabla();

        // Verificar que haya productos vendidos
        if (productosVendidos.isEmpty()) {
            ventanaError.renderMensaje("No se han vendido productos");
            return;
        }

        // Obtener el cliente seleccionado o crear uno nuevo si no hay cliente seleccionado
        Cliente cliente = obtenerClientePorDNI();

        // Obtener el total de la venta
        double totalVenta = calcularTotalVenta(productosVendidos);

        // Llamar al método agregarVenta del VentaController
        boolean error = ventaController.agregarVenta(cliente, productosVendidos, totalVenta);
        if (!error) {
            // La venta se realizó con éxito, puedes mostrar un mensaje o realizar alguna acción
            // después de la venta exitosa.
            // Por ejemplo:
            String format = generarBoletaVenta(clienteMain.getNombre(), clienteMain.getDni(),productosVendidos, totalVenta);
            
            impresion.renderMensaje(format);
            
            limpiar();
        }
    }//GEN-LAST:event_btnVoucherActionPerformed

    private void txtDniKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDniKeyTyped
        char c = evt.getKeyChar();
        if (Character.isLetter(c)) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "No se Aceptan Letras");
        }

        if (txtDni.getText().trim().length() == 8) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo se acepta 08 digitos");
        }
    }//GEN-LAST:event_txtDniKeyTyped

    private void txtCantidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyTyped
        char c = evt.getKeyChar();
        if (Character.isLetter(c)) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "No se Aceptan Letras");
        }

        if (txtCantidad.getText().trim().length() == 5) {
            evt.consume();
            JOptionPane.showMessageDialog(null, "Ingrese cantidad razonable");
        }
    }//GEN-LAST:event_txtCantidadKeyTyped

    private List<ProductoVendido> obtenerProductosVendidosDeTabla() {
        List<ProductoVendido> productosVendidos = new ArrayList<>();
        DefaultTableModel model = (DefaultTableModel) table.getModel();

        for (int i = 0; i < model.getRowCount(); i++) {
            String codigoProducto = (String) model.getValueAt(i, 0);
            int cantidad = (int) model.getValueAt(i, 4);

            // Verificar si la cantidad es mayor que 0
            if (cantidad > 0) {
                // Agregar el producto vendido a la lista
                productosVendidos.add(new ProductoVendido(codigoProducto, cantidad));
            }
        }

        return productosVendidos;
    }

    private double calcularTotalVenta(List<ProductoVendido> productosVendidos) {
        double total = 0;

        for (ProductoVendido productoVendido : productosVendidos) {
            // Obtener el producto actual de la base de datos (puedes usar el método obtenerProductoPorCodigo)
            Producto producto = obtenerProductoPorCodigo();

            if (producto != null) {
                // Calcular el subtotal (precio * cantidad)
                double subtotal = producto.getPrecio() * productoVendido.getCantidad();
                // Sumar el subtotal al total
                total += subtotal;
            }
        }

        return total;
    }

    public Cliente obtenerClientePorDNI() {
        // Lógica para obtener la lista de clientes a través del controlador
        return clienteController.obtenerClientePorDNI(txtDni.getText());
    }

    public Producto obtenerProductoPorCodigo() {
        // Lógica para obtener la lista de clientes a través del controlador
        return ProductoController.obtenerProductoPorCodigo(txtCode.getText());
    }

    public int convertirStringAInt(String numeroStr) {
        return productoController.convertirStringAInt(numeroStr);
    }

    public double convertirStringADouble(String numeroStr) {
        return productoController.convertirStringADouble(numeroStr);
    }

    private void eliminarUltimaFila() {
        DefaultTableModel model = (DefaultTableModel) table.getModel();

        int ultimaFila = model.getRowCount() - 1;
        if (ultimaFila >= 0) {
            double precioTotal = cantidadMain * productoMain.getPrecio();
            System.out.println(precioTotal);            
            totalMain -= precioTotal;
            labelTotal.setText(Double.toString(totalMain));
            
            if (ultimaFila == 0) {
                btnVoucher.setEnabled(false);
            }
            model.removeRow(ultimaFila);
        } else {
            ventanaError.renderMensaje("La tabla está vacía");
        }
    }

    public String generarBoletaVenta(String nombreCliente, String dni, List<ProductoVendido> productos, double totalVenta) {
        StringBuilder boletaBuilder = new StringBuilder();

        // Encabezado de la boleta
        boletaBuilder.append("======== BOLETA DE VENTA ========\n");
        boletaBuilder.append("Cliente: ").append(nombreCliente).append("DNI: ").append(dni).append("\n");
        boletaBuilder.append("=================================\n");
        boletaBuilder.append(String.format("%-15s%-25s%-10s%-10s%-10s\n",
            "Código", "Nombre Producto", "Cantidad", "Precio", "Subtotal"));

        // Detalles de productos y cálculo de subtotal
        double subtotal;
        double total = 0.0;
        for (ProductoVendido productoVendido : productos) {
            String codigo = productoVendido.getCodigoProducto();
            int cantidad = productoVendido.getCantidad();

            // Aquí asumimos que los productos están almacenados en una base de datos u otra fuente
            // y se puede acceder a los detalles del producto a partir del código del producto.
            // Si es necesario, puedes modificar esta parte para obtener el nombre y precio del producto
            // desde la base de datos o alguna otra fuente.
            String nombreProducto = "Nombre del Producto"; // Reemplazar por el nombre real del producto
            double precio = 10.0; // Reemplazar por el precio real del producto

            subtotal = cantidad * precio;
            total += subtotal;

            boletaBuilder.append(String.format("%-15s%-25s%-10d%-10.2f%-10.2f\n",
                codigo, nombreProducto, cantidad, precio, subtotal));
        }

        // Línea separadora
        boletaBuilder.append("=================================\n");

        // Total de la venta
        boletaBuilder.append(String.format("%-45s%-10.2f\n", "TOTAL VENTA:", totalVenta));
        boletaBuilder.append("=================================\n");

        return boletaBuilder.toString();
    }
    
    private void limpiar() {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);

        txtCode.setText("");
        txtCantidad.setText("");
        txtCode.setEnabled(false);
        btnValidarProducto.setEnabled(false);
        txtCantidad.setEnabled(false);
        btnValidarCantidad.setEnabled(false);
        btnAgregar.setEnabled(false);
        btnQuitar.setEnabled(false);
        btnVoucher.setEnabled(false);
        labelCliente.setText("Nombre Del Cliente");
        labelProducto.setText("Nombre Del Producto");
        labelPrecio.setText("Precio del Producto");
        labelPrecioTotal.setText("Precio Total del Producto");
        labelStock.setText("Stock Del Producto");
        labelTotal.setText("0");
        btnValidarCliente.setEnabled(true);
        txtDni.setEnabled(true);
    
        productoMain = null;
        clienteMain = null;
        cantidadMain = 0;
        totalMain = 0;
    }

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jfrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jfrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jfrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jfrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new jfrmVentas(usuario, clienteController, empleadoController, productoController, ventaController).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnQuitar;
    private javax.swing.JButton btnValidarCantidad;
    private javax.swing.JButton btnValidarCliente;
    private javax.swing.JButton btnValidarProducto;
    private javax.swing.JButton btnVoucher;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelCliente;
    private javax.swing.JLabel labelPrecio;
    private javax.swing.JLabel labelPrecioTotal;
    private javax.swing.JLabel labelProducto;
    private javax.swing.JLabel labelStock;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JLabel lblLogo;
    private javax.swing.JLabel lblSalir;
    private javax.swing.JTable table;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCode;
    private javax.swing.JTextField txtDni;
    // End of variables declaration//GEN-END:variables
}
