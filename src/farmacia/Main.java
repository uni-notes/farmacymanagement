package farmacia;

import farmacia.controlador.LoginController;


import farmacia.db.DB;
import farmacia.db.EmpleadoDAO;
import farmacia.db.ClienteDAO;
import farmacia.db.ProductoDAO;
import farmacia.db.VentaDAO;

import farmacia.controlador.ClienteController;
import farmacia.controlador.EmpleadoController;
import farmacia.controlador.ProductoController;
import farmacia.controlador.VentaController;

public class Main {
    public static void main(String[] args) {
                
        // Declaracion de entidades
        
        DB myDB = new DB();
        
        EmpleadoDAO empleadoDB = new EmpleadoDAO(myDB.getConnection());
        ClienteDAO clienteDB = new ClienteDAO(myDB.getConnection());
        ProductoDAO productoDB = new ProductoDAO(myDB.getConnection());
        VentaDAO ventaDB = new VentaDAO(myDB.getConnection());
        
        ClienteController clienteController = new ClienteController(clienteDB);
        EmpleadoController empleadoController = new EmpleadoController(empleadoDB);
        ProductoController productoController = new ProductoController(productoDB);
        VentaController ventaController = new VentaController(productoDB, ventaDB);
        
        
        LoginController loginController = new LoginController(clienteController, empleadoController, productoController, ventaController,empleadoDB);


        boolean isConnectionActive = myDB.ping();
        if (isConnectionActive) {
            System.out.println("La conexión está activa.");
        } else {
            System.out.println("La conexión no está activa.");
        }
        
        myDB.initDB();
        
        loginController.mostrarVentana();              
    }
}
