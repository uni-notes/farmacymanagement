/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package farmacia.controlador;

import farmacia.modelo.personas.Cliente;

import farmacia.db.ClienteDAO;

import farmacia.vista.Error;

import java.sql.SQLException;
import java.util.List;


/**
 * Controlador de clientes
 * Maneja las operaciones relacionadas con los clientes en la farmacia.
 * Se encarga de interactuar con la capa de acceso a datos (ClienteDAO) y la vista de errores (Error).
 * Implementa buenas prácticas de desarrollo.
 * 
 * @author cfabrica
 */
public class ClienteController {

    private static ClienteDAO clienteDB;
    private static Error ventanaError;
    
    /**
     * Constructor de ClienteController.
     * Inicializa el ClienteDAO y la ventana de errores.
     * 
     * @param clienteDB Objeto ClienteDAO para interactuar con la capa de acceso a datos.
     */
    public ClienteController(ClienteDAO clienteDB) {
        ClienteController.clienteDB = clienteDB;
        ClienteController.ventanaError = new Error();
    }
    
    /**
     * Obtiene todos los clientes de la base de datos.
     * 
     * @return Lista de objetos Cliente si se encuentran registros en la base de datos,
     *         o null si la tabla de clientes está vacía.
     */
    public List<Cliente> obtenerTodosLosClientes(){
        
         try {
            List<Cliente> clientes = clienteDB.obtenerTodosLosClientes();
            if (clientes != null) {
               return clientes;
            } else {
                ventanaError.renderMensaje("Tabla clientes vacía");
            }
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        
        return null;
    }
    
    /**
     * Obtiene un cliente por su número de DNI.
     * 
     * @param dni Número de DNI del cliente a buscar.
     * @return Objeto Cliente si se encuentra en la base de datos,
     *         o null si no se encuentra.
     */
    public Cliente obtenerClientePorDNI(String dni){     
         try {
            Cliente cliente = clienteDB.obtenerClientePorDni(dni);
            if (cliente != null) {
               return cliente;
            } else {
                ventanaError.renderMensaje("No se encontró el cliente");
            }
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        
        return null;
    }
    
    /**
     * Inserta un nuevo cliente en la base de datos.
     * 
     * @param dni Número de DNI del cliente.
     * @param nombres Nombres del cliente.
     * @param apellidos Apellidos del cliente.
     * @param telefono Teléfono del cliente.
     * @param direccion Dirección del cliente.
     * @param correo Correo electrónico del cliente.
     * @return true si hay error, false si no hay error.
     */
    public boolean InsertarCliente(String dni, String nombres, String apellidos, String telefono, String direccion, String correo){    
        if (!camposValidos(dni, nombres, apellidos, telefono, direccion, correo)) {
            return true;
        }
        
        try {
             // Crear una instancia del cliente
            Cliente cliente = new Cliente(dni, nombres, apellidos, telefono, direccion, correo);
            
            clienteDB.agregarCliente(cliente); 
            
            return false;
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        
        return true;
    }
    
    /**
     * Actualiza un cliente en la base de datos.
     * 
     * @param dni Número de DNI del cliente.
     * @param nombres Nombres del cliente.
     * @param apellidos Apellidos del cliente.
     * @param telefono Teléfono del cliente.
     * @param direccion Dirección del cliente.
     * @param correo Correo electrónico del cliente.
     * @return true si hay error, false si no hay error.
     */
    public boolean ActualizarCliente(String dni, String nombres, String apellidos, String telefono, String direccion, String correo){    
        if (!camposValidos(dni, nombres, apellidos, telefono, direccion, correo)) {
            return true;
        }
        
        try {
             // Crear una instancia del cliente
            Cliente cliente = new Cliente(dni, nombres, apellidos, telefono, direccion, correo);
            
            clienteDB.actualizarCliente(cliente);
            
            return false;
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        
        return true;
    }
    
    /**
     * Elimina un cliente de la base de datos por su número de DNI.
     * 
     * @param dni Número de DNI del cliente a eliminar.
     */
    public void DeleteCliente(String dni){
        if (!validarDni(dni)) {
            return;
        }
        
         try {
            clienteDB.eliminarCliente(dni);
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
    }
    
    /**
     * Valida los campos requeridos para crear un cliente.
     * 
     * @param dni Número de DNI del cliente.
     * @param nombres Nombres del cliente.
     * @param apellidos Apellidos del cliente.
     * @param telefono Teléfono del cliente.
     * @param direccion Dirección del cliente.
     * @param correo Correo electrónico del cliente.
     * @return true si todos los campos están completos, false si alguno de ellos está vacío.
     */
    public boolean camposValidos(String dni, String nombres, String apellidos, String telefono, String direccion, String correo) {
        
        if (!validarDni(dni)){
            return false;
        }
        
        if (!telefono.matches("\\d+")) {
            ventanaError.renderMensaje("El Telefono debe contener solo dígitos");
            return false;
        }
        
        if (nombres.isEmpty() || apellidos.isEmpty() || telefono.isEmpty() || direccion.isEmpty() || correo.isEmpty()) {
            ventanaError.renderMensaje("Algunos campos están vacíos");
            return false;
        }
        return true;
    }
    
    /**
     * Valida el campo de número de DNI.
     * 
     * @param dni Número de DNI del cliente.
     * @return true si el campo está completo, false si está vacío.
     */
    public boolean validarDni(String dni) {
        if (dni.isEmpty()) {
            ventanaError.renderMensaje("El campo de número de DNI está vacío");
            return false;
        }
        
        if (dni.length() != 8) {
            ventanaError.renderMensaje("El DNI debe tener 8 dígitos");
            return false;
        }
    
        if (!dni.matches("\\d+")) {
            ventanaError.renderMensaje("El DNI debe contener solo dígitos");
            return false;
        }
        return true;
    }
    
    /**
     * Método placeholder para funcionalidad futura.
     * Muestra un mensaje de error genérico en la ventana de errores.
     */
    public void proximamente() {
        ventanaError.renderMensaje("Funcionalidad próximamente disponible");
    }
}
