package farmacia.controlador;

import farmacia.db.InventarioDAO;
import farmacia.modelo.Inventario;
import java.sql.SQLException;

public class InventarioController {
    private InventarioDAO inventarioDAO;

    public InventarioController(InventarioDAO inventarioDAO) {
        this.inventarioDAO = inventarioDAO;
    }

    public Inventario obtenerInventarioPorCodigoProducto(String codigoProducto) throws SQLException {
        return inventarioDAO.obtenerInventarioPorCodigoProducto(codigoProducto);
    }
}