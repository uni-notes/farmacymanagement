package farmacia.controlador;

import farmacia.vista.jfrmLogueo;
import farmacia.vista.jfrmMenu;
import farmacia.vista.Error;

import farmacia.db.EmpleadoDAO;

import farmacia.modelo.personas.Empleado;

import java.sql.SQLException;

/**
 * Controlador de login
 * Maneja el proceso de inicio de sesión y gestiona las ventanas relacionadas.
 * Implementa buenas prácticas de desarrollo.
 * Utiliza el controlador de empleados (EmpleadoController) para obtener los datos de los empleados.
 * Utiliza la vista de errores (Error) para mostrar mensajes de error.
 * Utiliza el acceso a datos (EmpleadoDAO) para obtener los empleados de la base de datos.
 * 
 */
public class LoginController {
    private static jfrmLogueo ventanaLogueo;
    private static Error ventanaError;
    private static EmpleadoDAO empleadoDB;
    
    private static ClienteController clienteController;
    private static EmpleadoController empleadoController;
    private static ProductoController productoController;
    private static VentaController ventaController;
    

    /**
     * Constructor de LoginController.
     * Inicializa la ventana de logueo, la ventana de errores y los controladores de cliente, empleado y producto.
     * 
     * @param clienteController Controlador de clientes.
     * @param empleadoController Controlador de empleados.
     * @param productoController Controlador de productos.
     * @param empleadoDB Objeto EmpleadoDAO para interactuar con la capa de acceso a datos.
     */
    public LoginController(ClienteController clienteController, EmpleadoController empleadoController, ProductoController productoController, VentaController ventaController,EmpleadoDAO empleadoDB) {
        LoginController.ventanaLogueo = new jfrmLogueo();
        LoginController.ventanaError = new Error();
        LoginController.empleadoDB = empleadoDB;
        LoginController.clienteController = clienteController;
        LoginController.empleadoController = empleadoController;
        LoginController.productoController = productoController;
        LoginController.ventaController = ventaController;
    }

    /**
     * Muestra la ventana de logueo.
     */
    public void mostrarVentana() {
        ventanaLogueo.setVisible(true);
    }
    
    /**
     * Realiza el proceso de inicio de sesión.
     * Verifica las credenciales proporcionadas por el usuario y muestra la ventana de menú si son correctas.
     * 
     * @param usuario Nombre de usuario ingresado.
     * @param contrasena Contraseña ingresada.
     * @return true si el inicio de sesión es exitoso, false de lo contrario.
     */
    public static boolean login(String usuario, String contrasena) {
        
        try {
            Empleado empleado = empleadoDB.obtenerEmpleadoPorCredenciales(usuario, contrasena); // Llama al método obtenerEmpleadoPorCredenciales
            if (empleado != null) {
                jfrmMenu ventanaMenu = new jfrmMenu(empleado.getNombre() ,clienteController, empleadoController, productoController, ventaController);
                ventanaMenu.setVisible(true);
                ventanaLogueo.cerrarVentana();
                return true;
            } else {
                ventanaError.renderMensaje("Usuario y/o contraseña incorrectos");
            }
        } catch (SQLException e) {
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        return false;
    }
}