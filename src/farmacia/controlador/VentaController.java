package farmacia.controlador;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import farmacia.db.VentaDAO;
import farmacia.db.ProductoDAO;

import farmacia.modelo.ProductoVendido;
import farmacia.modelo.Producto;
import farmacia.modelo.Venta;
import farmacia.modelo.personas.Cliente;
import farmacia.vista.Error;

/**
 * Controlador de ventas.
 * Maneja las operaciones relacionadas con las ventas en la farmacia.
 * Se encarga de interactuar con la capa de acceso a datos (VentaDAO) y la vista de errores (Error).
 * Implementa buenas prácticas de desarrollo.
 * 
 * @author cfabrica
 */
public class VentaController {

    private static VentaDAO ventaDB;
    private static ProductoDAO productoDB;
    private static Error ventanaError;
    
    /**
     * Constructor de VentaController.
     * Inicializa el VentaDAO y la ventana de errores.
     * 
     * @param connection Objeto Connection para la conexión a la base de datos.
     */
    public VentaController(ProductoDAO productoDB, VentaDAO ventaDB) {
        VentaController.ventaDB = ventaDB;
        VentaController.productoDB = productoDB;
        ventanaError = new Error();
    }
    
    
    /**
    * Agrega una nueva venta a la base de datos.
    *
    * @param cliente Cliente que realiza la venta.
    * @param productosVendidos Lista de productos vendidos en la venta.
    * @param total Total de la venta.
    * @return true si hay error, false si no hay error.
    */
    public boolean agregarVenta(Cliente cliente, List<ProductoVendido> productosVendidos, double total) {
        try {
            // Crear una nueva instancia de Venta con el cliente, productos vendidos y total
            Venta venta = new Venta(cliente, productosVendidos, total);

            // Actualizar la cantidad de productos vendidos en la tabla de productos
            for (ProductoVendido productoVendido : productosVendidos) {
                String codigoProducto = productoVendido.getCodigoProducto();
                int cantidadVendida = productoVendido.getCantidad();

                // Obtener el producto actual de la base de datos
                Producto producto = productoDB.obtenerProductoPorCodigo(codigoProducto);

                // Verificar si el producto existe en la base de datos
                if (producto != null) {
                    // Actualizar la cantidad de productos restando la cantidad vendida
                    int nuevaCantidad = producto.getCantidad() - cantidadVendida;

                    // Verificar que la cantidad no sea negativa
                    if (nuevaCantidad >= 0) {
                        producto.setCantidad(nuevaCantidad);
                        // Actualizar la cantidad del producto en la base de datos
                        productoDB.actualizarProducto(producto);
                    } else {
                        ventanaError.renderMensaje("No hay suficiente cantidad del producto " + producto.getNombre() +
                                " para realizar la venta");
                        return true;
                    }
                } else {
                    ventanaError.renderMensaje("El producto con código " + codigoProducto + " no existe en la base de datos");
                    return true;
                }
            }

            // Agregar la venta a la base de datos
            ventaDB.agregarVenta(venta);

            return false;
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
            System.out.println(e.toString());
        }

        return true;
    }

    /**
     * Actualiza una venta en la base de datos.
     * 
     * @param venta Venta actualizada.
     * @return true si hay error, false si no hay error.
     */
    public boolean actualizarVenta(Venta venta) {
        try {
            ventaDB.actualizarVenta(venta);
            return false;
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        
        return true;
    }
    
    /**
     * Elimina una venta de la base de datos por su ID.
     * 
     * @param id ID de la venta a eliminar.
     */
    public void eliminarVenta(int id) {
        try {
            ventaDB.eliminarProductosVendidosPorVentaId(id);
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
    }
}
