/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package farmacia.controlador;

import farmacia.db.ProductoDAO;

import farmacia.vista.Error;

import farmacia.modelo.Producto;

import java.sql.SQLException;
import java.util.List;

/**
 * Controlador de productos.
 * Maneja las operaciones relacionadas con los productos, como obtener, agregar y eliminar productos.
 * Implementa buenas prácticas de desarrollo.
 * Utiliza el controlador de errores (Error) para mostrar mensajes de error.
 * Utiliza el acceso a datos (ProductoDAO) para interactuar con la capa de acceso a la base de datos.
 */
public class ProductoController {
    private static ProductoDAO productoDB;
    private static Error ventanaError;


    /**
     * Constructor de ProductoController.
     * Inicializa el objeto ProductoDAO y la ventana de errores.
     * 
     * @param productoDB Objeto ProductoDAO para interactuar con la capa de acceso a datos.
     */
    public ProductoController(ProductoDAO productoDB) {
        ProductoController.productoDB = productoDB;
        ProductoController.ventanaError = new Error();
    }
    
    /**
     * Obtiene todos los productos de la base de datos.
     * 
     * @return Lista de productos si existen, null si la tabla de productos está vacía.
     */
    public static List<Producto> obtenerTodosLosProductos(){
        
         try {
            List<Producto> productos = productoDB.obtenerTodosLosProductos();
            if (productos != null) {
               return productos;
            } else {
               ventanaError.renderMensaje("Tabla productos vacía");
            }
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        
        return null;
    }
    
    /**
     * Obtiene un producto por su código.
     * 
     * @param codigo Código del producto a buscar.
     * @return Producto si se encuentra, null si no se encuentra.
     */
    public static Producto obtenerProductoPorCodigo(String codigo){
        
         try {
            Producto producto = productoDB.obtenerProductoPorCodigo(codigo);
            if (producto != null) {
               return producto;
            } else {
                ventanaError.renderMensaje("No se encontró el Producto");
            }
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        
        return null;
    }
    
    /**
     * Agrega un nuevo producto a la base de datos.
     * 
     * @param codigo
     * @param nombre
     * @param precio
     * @param cantidad
     * @return 
     */
    public static boolean InsertarProducto(String codigo, String nombre, double precio, int cantidad){        
        if (!camposValidos(codigo, nombre, precio, cantidad)) {
            return true;
        }
        
        Producto producto = new Producto(codigo, nombre, precio, cantidad);
        
        try {
            productoDB.agregarProducto(producto);
            return false;
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        return true;
    }
    
    /**
     * Actualiza un producto a la base de datos.
     * 
     * @param codigo
     * @param nombre
     * @param precio
     * @param cantidad
     * @return 
     */
    public static boolean ActualizarProducto(String codigo, String nombre, double precio, int cantidad){        
        if (!camposValidos(codigo, nombre, precio, cantidad)) {
            return true;
        }
        
        Producto producto = new Producto(codigo, nombre, precio, cantidad);
        
        try {
            productoDB.actualizarProducto(producto);
            return false;
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        return true;
    }
    
    /**
     * Elimina un producto de la base de datos por su código.
     * 
     * @param codigo Código del producto a eliminar.
     */
    public static void DeleteProducto(String codigo){    
        if (!validarCodigo(codigo)) {
            return;
        }
        
        try {
            productoDB.eliminarProducto(codigo);
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
    }
    
    /**
     * Valida los campos del producto.
     * Verifica que el código y el nombre no estén vacíos.
     * Verifica que el precio sea mayor a 0 y la cantidad mayor o igual a 0.
     * 
     * @param codigo Código del producto.
     * @param nombre Nombre del producto.
     * @param precio Precio del producto.
     * @param cantidad Cantidad del producto.
     * @return true si los campos son válidos, false de lo contrario.
     */
    public static boolean camposValidos(String codigo, String nombre, double precio, int cantidad) {
        if (codigo.isEmpty() || nombre.isEmpty()) {
            ventanaError.renderMensaje("Algunos campos están vacíos");
            return false;
        }
        
        if(precio <= 0){
            ventanaError.renderMensaje("Precio no puede ser menor o igual a 0");
            return false;
        }
        
        if(cantidad < 0){
            ventanaError.renderMensaje("Cantidad no puede ser menor a 0");
            return false;
        }
        return true;
    }
    
    /**
     * Valida el código del producto.
     * Verifica que el código no esté vacío.
     * 
     * @param codigo Código del producto.
     * @return true si el código es válido, false de lo contrario.
     */
    public static boolean validarCodigo(String codigo) {
        if (codigo.isEmpty()) {
            ventanaError.renderMensaje("El código está vacío");
            return false;
        }
        return true;
    }
    
    /**
     * Método de ejemplo.
     * Muestra un mensaje en la ventana de errores.
     */
    public void proximamente() {
        ventanaError.renderMensaje("Próximamente");
    }
    
    /**
     * Convierte una cadena de texto a un valor de tipo double.
     * 
     * @param numeroStr Cadena de texto a convertir.
     * @return Valor double si la conversión es exitosa, 0 si la cadena no es un número válido.
     */
    public double convertirStringADouble(String numeroStr) {
        double numero;

        try {
            numero = Double.parseDouble(numeroStr);
        } catch (NumberFormatException e) {
            // El formato del String no es válido
            // Aquí puedes manejar el error, mostrar un mensaje, etc.
            ventanaError.renderMensaje("El precio debe ser un número");
            // Asignar un valor por defecto o lanzar una excepción, según lo que necesites
            numero = 0;
        }

        return numero;
    }
    
    /**
     * Convierte una cadena de texto a un valor de tipo int.
     * 
     * @param numeroStr Cadena de texto a convertir.
     * @return Valor int si la conversión es exitosa, -1 si la cadena no es un número válido.
     */
    public int convertirStringAInt(String numeroStr) {
        int numero;

        try {
            numero = Integer.parseInt(numeroStr);
        } catch (NumberFormatException e) {
            // El formato del String no es válido
            // Aquí puedes manejar el error, mostrar un mensaje, etc.
            ventanaError.renderMensaje("La cantidad debe ser un número entero");
            // Asignar un valor por defecto o lanzar una excepción, según lo que necesites
            numero = -1;
        }

        return numero;
    }
}