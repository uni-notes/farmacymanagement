/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package farmacia.controlador;

import farmacia.modelo.personas.Empleado;

import farmacia.vista.Error;

import farmacia.db.EmpleadoDAO;
import farmacia.modelo.personas.CredencialesEmpleado;


import java.sql.SQLException;
import java.util.List;

/**
 * Controlador de empleados
 * Maneja las operaciones relacionadas con los empleados en la farmacia.
 * Se encarga de interactuar con la capa de acceso a datos (EmpleadoDAO) y la vista de errores (Error).
 * Implementa buenas prácticas de desarrollo.
 * 
 * @author cfabrica
 */
public class EmpleadoController {

    private static EmpleadoDAO empleadoDB;
    private static Error ventanaError;

    
    /**
     * Constructor de EmpleadoController.
     * Inicializa el EmpleadoDAO y la ventana de errores.
     * 
     * @param empleadoDB Objeto EmpleadoDAO para interactuar con la capa de acceso a datos.
     */
    public EmpleadoController(EmpleadoDAO empleadoDB) {
        EmpleadoController.empleadoDB = empleadoDB;
        EmpleadoController.ventanaError = new Error();
    }
    
    /**
     * Obtiene todos los empleados de la base de datos.
     * 
     * @return Lista de objetos Empleado si se encuentran registros en la base de datos,
     *         o null si la tabla de empleados está vacía.
     */
    public List<Empleado> obtenerTodosLosEmpleados(){
         try {
            List<Empleado> empleados = empleadoDB.obtenerTodosLosEmpleados();
            if (empleados != null) {
               return empleados;
            } else {
                ventanaError.renderMensaje("Tabla empleados vacía");
            }
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        
        return null;
    }
    
    /**
     * Obtiene un empleado por su número de DNI.
     * 
     * @param dni Número de DNI del empleado a buscar.
     * @return Objeto Empleado si se encuentra en la base de datos,
     *         o null si no se encuentra.
     */
    public Empleado obtenerEmpleadoPorDNI(String dni){  
         try {
            Empleado empleado = empleadoDB.obtenerEmpleadoPorDni(dni);
            if (empleado != null) {
               return empleado;
            } else {
                ventanaError.renderMensaje("No se encontró el empleado");
            }
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
        
        return null;
    }
    
    /**
     * @param dni Número de DNI del empleado.
     * @param nombres Nombres del empleado.
     * @param apellidos Apellidos del empleado.
     * @param salario Salario del empleado.
     * @param telefono Telefono del empleado.
     * @param cargo Cargo del empleado.
     * @param contrasena Contraseña del empleado.
     * @return true si hay error, false si no hay error.
     */
    public boolean InsertarEmpleado(String dni, String nombres, String apellidos, String telefono,double salario, String cargo, String contrasena){   
        if (!camposValidos(dni, nombres, apellidos, telefono , salario, cargo, contrasena)) {
            return true;
        }
        
        CredencialesEmpleado credenciales = new CredencialesEmpleado(dni,contrasena);

        Empleado empleado = new Empleado(dni, nombres, apellidos, telefono, salario, cargo, credenciales);
        
        try {
            empleadoDB.agregarEmpleado(empleado);  
            return true; 
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
         
        return true;
    }
    
    /**
     * @param dni Número de DNI del empleado.
     * @param nombres Nombres del empleado.
     * @param apellidos Apellidos del empleado.
     * @param salario Salario del empleado.
     * @param telefono Telefono del empleado.
     * @param cargo Cargo del empleado.
     * @param contrasena Contraseña del empleado.
     * @return true si hay error, false si no hay error.
     */
    public boolean ActualizarEmpleado(String dni, String nombres, String apellidos, String telefono,double salario, String cargo, String contrasena){   
        if (!camposValidos(dni, nombres, apellidos, telefono , salario, cargo, contrasena)) {
            return true;
        }
        
        CredencialesEmpleado credenciales = new CredencialesEmpleado(dni,contrasena);

        Empleado empleado = new Empleado(dni, nombres, apellidos, telefono, salario, cargo, credenciales);
        
        try {
            empleadoDB.actualizarEmpleado(empleado);  
            return true; 
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
         
        return true;
    }
    
    /**
     * Elimina un empleado de la base de datos por su número de DNI.
     * 
     * @param dni Número de DNI del empleado a eliminar.
     */
    public void DeleteEmpleado(String dni){    
        if (!validarDni(dni)) {
            return;
        }
       
        try {
            empleadoDB.eliminarEmpleado(dni);
        } catch (SQLException e) {
            // Ocurrió un error al ejecutar la consulta SQL
            ventanaError.renderMensaje("Error en la base de datos: " + e.toString());
        }
    }
    
    /**
     * Valida los campos requeridos para crear un empleado.
     * 
     * @param dni Número de DNI del empleado.
     * @param nombres Nombres del empleado.
     * @param apellidos Apellidos del empleado.
     * @param telefono Telefono del empleado.
     * @param salario Salario del empleado.
     * @param cargo Cargo del empleado.
     * @param contrasena Contraseña del empleado.
     * @return true si todos los campos están completos y el salario es mayor a cero, false si alguno de ellos está vacío o el salario es menor o igual a cero.
     */
    public boolean camposValidos(String dni, String nombres, String apellidos, String telefono ,double salario, String cargo, String contrasena) {
        if (!validarDni(dni)){
            return false;
        }
        
        if (!telefono.matches("\\d+")) {
            ventanaError.renderMensaje("El Telefono debe contener solo dígitos");
            return false;
        }
        
        if (nombres.isEmpty() || apellidos.isEmpty() || telefono.isEmpty() || cargo.isEmpty() || contrasena.isEmpty()) {
            ventanaError.renderMensaje("Algunos campos están vacíos");
            return false;
        }
        
        if(salario <= 0){
            ventanaError.renderMensaje("El salario no puede ser menor o igual a cero");
            return false;
        }
        return true;
    }
    
    /**
     * Valida el campo de número de DNI.
     * 
     * @param dni Número de DNI del empleado.
     * @return true si el campo está completo, false si está vacío.
     */
    public boolean validarDni(String dni) {
        if (dni.isEmpty()) {
            ventanaError.renderMensaje("El campo de número de DNI está vacío");
            return false;
        }
        
        if (dni.length() != 8) {
            ventanaError.renderMensaje("El DNI debe tener 8 dígitos");
            return false;
        }
    
        if (!dni.matches("\\d+")) {
            ventanaError.renderMensaje("El DNI debe contener solo dígitos");
            return false;
        }
        
        return true;
    }
    
    /**
     * Método placeholder para funcionalidad futura.
     * Muestra un mensaje de error genérico en la ventana de errores.
     */
    public void proximamente() {
        ventanaError.renderMensaje("Funcionalidad próximamente disponible");
    }
    
    /**
     * Convierte un String en formato numérico a un valor double.
     * 
     * @param numeroStr String que representa el número.
     * @return Valor double del número, o 0 si el formato es inválido.
     */
    public double convertirStringADouble(String numeroStr) {
        double numero;

        try {
            numero = Double.parseDouble(numeroStr);
        } catch (NumberFormatException e) {
            // El formato del String no es válido
            // Aquí puedes manejar el error, mostrar un mensaje, etc.
            ventanaError.renderMensaje("El salario debe ser un número válido");
            // Asignar un valor por defecto o lanzar una excepción, según lo que necesites
            numero = 0;
        }

        return numero;
    }
}